# Exocci

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `exocci` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:exocci, "~> 0.1.0"}]
    end
    ```

  2. Ensure `exocci` is started before your application:

    ```elixir
    def application do
      [applications: [:exocci]]
    end
    ```

## TODO

* Use protocol for serialization

### Attributes

### Categories
